# Quero Minesweeper

## Dependências
- [Colorize](https://rubygems.org/gems/colorize/versions/0.8.1)

Ou se preferir, você pode instalar o [Bundler](http://bundler.io/) e executar o comando abaixo:
```sh
bundler install
```

## Como usar

Crie uma instância da classe Minesweeper::Game e atribua à uma variável, ex:
`````rb
x,y,num_mines = 50,50,250
game = Minesweeper::game.new(rows: x, cols: y, num_mines: num_mines)
`````
###### Parâmetros obrigatórios para ```` Minesweeper::game.new ````:
- `` rows ``: define a quantidade de células horizontais - `` int 1-100 ``
- `` cols ``: define a quantidade de células verticais - `` int 1-100 ``
- `` num_mines ``: define a quantidade de células minadas ( a quantidade de minas deve representar no mínimo 1% da quantidade total de células ) - `` int ``

## Métodos disponíveis para Minesweeper::Game
### Game.play
`````rb
valid_move = game.play(x: 10, y: 20)
`````
###### Definições e parâmetros de ```` Game.play ````:
- `` x ``: define uma posição horizontal - `` int 1-rows ``
- `` y ``: define uma posição vertical - `` int 1-cols ``
- `` return bool ``

Após escolher as coordenadas ``x`` e ``y`` a célula irá ser acionada, o método irá retornar ``true`` caso a célula não tenha nenhuma bandeira (``flag``) e não tenha sido escolhida anteriormente em outra jogada. O valor ``false`` é retornado quando a célula já tenha sido escolhida anteriormente, quando a célula tem uma bandeira e quando a partida não é mais válida seja por vitória ou por derrota.

### Game.flag
`````rb
valid_move = game.flag(x: 10, y: 20)
`````
###### Definições e parâmetros de ```` Game.flag ````:
- `` x ``: define uma posição horizontal - `` int 1-rows ``
- `` y ``: define uma posição vertical - `` int 1-cols ``
- `` return bool ``

Após escolher as coordenadas ``x`` e ``y`` a célula irá ser acionada e marcada com uma bandeira, caso não tenha uma. E sem bandeira, caso já tenha sido marcada anteriormente. Após isso, o método deve retornar o valor ``true``, indicando que a ação foi bem-sucedida. O valor ``false`` é retornado quando a célula já tenha sido escolhida anteriormente ou quando a partida não é mais válida seja por vitória ou por derrota.

### Game.board_state
`````rb
board_state = game.board_state(xray: true)
`````
###### Definições e parâmetros de ```` Game.board_state ````:
- `` xray ``: retorna a localização de todas as células minadas - `` true|false - opcional ``
- `` return hash ``

Caso seja informado o valor ``true``, o método apenas irá informar quais células são minadas caso a partida não seja mais válida seja por vitória ou derrota.

### Game.still_playing?
`````rb
game.still_playing?
`````
###### Definições e parâmetros de ```` Game.still_playing? ````:
- `` return bool ``

Retorna ``true`` caso o jogo ainda esteja em andamento.
Retorna ``false`` caso o jogo não seja mais válido, seja por ter alcançado a vitória ou derrota.

### Game.victory?
`````rb
game.victory?
`````
###### Definições e parâmetros de ```` Game.victory? ````:
- `` return bool ``

Retorna ``true`` caso a partida não seja mais válida e não haja células restantes para descobrir.
Retorna ``false`` caso a partida seja válida, quando há alguma bandeira em uma célula sem mina ou quando alguma célula com mina é descoberta.

## Printers

Há duas classes de printers disponíveis no projeto, exclusivas para o tratamento da exibição do método ``Game.board_state``. Uma das classes conta com uma representação de caracteres, e a outra com algumas informações essenciais.

### SimplePrinter.show
`````rb
SimplePrinter.show(game.board_state)
`````
###### Definições e parâmetros de ```` SimplePrinter.show ````:
- ``MineSweeper::Game.board_state hash``: hash de retorno do método board_state da classe MineSweeper::Game - ``hash``
- ``return output ``

### PrettyPrinter.show
`````rb
board_format = {
    unknown_cell: '.',
    clear_cell: ' ', # note que se este atributo estiver presente e diferente de nil, a célula descoberta não irá informar a quantidade de células minadas adjacentes à ela
    bomb: '#',
    flag: 'F'
}
PrettyPrinter.show(game.board_state, board_format)
`````
###### Definições e parâmetros de ```` PrettyPrinter.show ````:
- ``MineSweeper::Game.board_state hash``: hash de retorno do método board_state da classe MineSweeper::Game - ``hash``
- ``Board format hash``: substitui os caracteres padrões utilizados na representação de caracteres - ``hash - opcional``
- ``return output ``

## Exemplo de integração
````rb
require_relative 'lib/minesweeper/game'
require_relative 'lib/minesweeper/simple_printer'
require_relative 'lib/minesweeper/pretty_printer'

Game = Minesweeper::Game
SimplePrinter = Minesweeper::SimplePrinter
PrettyPrinter = Minesweeper::PrettyPrinter

x, y, num_mines = 50, 50, 250
game = Game.new(rows: x, cols: y, num_mines: num_mines)

while game.still_playing?
  valid_move = game.play(x: rand(x), y: rand(y))
  valid_flag = game.flag(x: rand(x), y: rand(y))
  if valid_move or valid_flag
  	SimplePrinter.show(game.board_state)
  end
end

puts "Fim do jogo!"
if game.victory?
  puts "Você venceu!"
  PrettyPrinter.show(game.board_state(xray: true))
else
  puts "Você perdeu!"
  PrettyPrinter.show(game.board_state(xray: true))
end
````

## Considerações finais à Quero
- Usei o método de Keyword Arguments em alguns métodos, para facilitar o entendimento e a integração
- A chamada dos printers.show foi alterada para chamadas estáticas para facilitar a integração