require_relative '../lib/minesweeper/game'
require_relative '../lib/minesweeper/pretty_printer'
require_relative '../lib/minesweeper/simple_printer'

Game = Minesweeper::Game
SimplePrinter = Minesweeper::SimplePrinter
PrettyPrinter = Minesweeper::PrettyPrinter

describe 'Minesweeper::Game' do


	it 'construct field with error' do
		expect { Game.new(rows: 101, cols: 100, num_mines: 100) }.to raise_error("rows must be greater than 0 and less than or equal to 100")
		expect { Game.new(rows: 100, cols: 101, num_mines: 100) }.to raise_error("cols must be greater than 0 and less than or equal to 100")
	end


	it 'click cell, invalid move' do

		@game = Game.new(rows: 10, cols: 10, num_mines: 10)

		cell_key = @game.send("get_cell_key", 1, 1)


		# Invalid game
		@game.instance_exec { @valid_game = false }
		expect(@game.play(x: 1, y: 1)).to eq(false)


		@game.instance_exec do 
			@valid_game = true
			@field[cell_key][:discovered] = true
		end
		expect(@game.play(x: 1, y: 1)).to eq(false)


		@game.instance_exec do 
			@field[cell_key][:discovered] = false
			@field[cell_key][:flag] = true
		end
		expect(@game.play(x: 1, y: 1)).to eq(false)


	end

	it 'click cell, valid move' do

		@game = Game.new(rows: 10, cols: 10, num_mines: 10)

		cell_key = @game.send("get_cell_key", 1, 1)

		field = @game.instance_exec do 
			@field[cell_key][:mine] = false
			
			@field[cell_key][:neighbors].each do |neighbor_cell_key|
				@field[neighbor_cell_key][:mine] = false
			end

			@field[cell_key][:adjacent_mines_count] = 0

			@field
		end

		@game.play(x: 1, y: 1)

		expect(field[cell_key][:discovered]).to eq(true)

		field[cell_key][:neighbors].each do |neighbor_cell_key|
			expect(field[neighbor_cell_key][:discovered]).to eq(true)
		end




		@game.instance_exec do 
			@field[cell_key][:discovered] = false 
			@field[cell_key][:mine] = true 
		end
		@game.play(x: 1, y: 1)

		expect(field[cell_key][:discovered]).to eq(true)
		expect(@game.still_playing?).to eq(false)



		# expect(@game.board_state[:valid_discoveries]).to eq(1)


		# @game.instance_exec do 
		# 	@field[cell_key][:mine] = true
		# end
		# expect(@game.play(x: 1, y: 1)).to eq(false)


	end

	it 'victory' do

		@game = Game.new(rows: 5, cols: 5, num_mines: 2)

		@game.instance_exec do
			@field.each do |cell|
				play(x: cell[:x], y: cell[:y]) if cell[:mine].eql?(false) && cell[:discovered].eql?(false)
			end
		end

		expect(@game.victory?).to eq(true)

	end

end