require_relative 'lib/minesweeper/game'
require_relative 'lib/minesweeper/simple_printer'
require_relative 'lib/minesweeper/pretty_printer'

Game = Minesweeper::Game
SimplePrinter = Minesweeper::SimplePrinter
PrettyPrinter = Minesweeper::PrettyPrinter

x, y, num_mines = 50, 50, 250
game = Game.new(rows: x, cols: y, num_mines: num_mines)

# Manual Play
# PrettyPrinter.show(game.board_state)
# while game.still_playing?
# 	print "X:".blue
# 	x = Integer(gets)
# 	print "Y:".blue
# 	y = Integer(gets)
# 	puts "\n"
# 	game.play(x: x, y: y)
# 	SimplePrinter.show(game.board_state)
# 	PrettyPrinter.show(game.board_state)
# end

# Automatic Play
while game.still_playing?
  valid_move = game.play(x: rand(x), y: rand(y))
  valid_flag = game.flag(x: rand(x), y: rand(y))
  if valid_move or valid_flag
  	SimplePrinter.show(game.board_state)
  end
end

puts "Fim do jogo!"
if game.victory?
  puts "Você venceu!"
  PrettyPrinter.show(game.board_state(xray: true))
else
  puts "Você perdeu!\n"
  PrettyPrinter.show(game.board_state(xray: true))
  SimplePrinter.show(game.board_state(xray: true))
end