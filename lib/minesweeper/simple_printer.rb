require 'colorize'

module Minesweeper
	class SimplePrinter
		def self.show(board_state)

			last_cell = board_state[:last_cell]
			valid_discoveries_count = board_state[:valid_discoveries]
			flags_count = board_state[:flags]
			num_cels = board_state[:num_cels]
			num_mines = board_state[:num_mines]
			remaining_cells = (num_cels - num_mines) - valid_discoveries_count

			arr_mined_cells = board_state[:field].select do |cell|
				cell[:mine] == true
			end

			if not last_cell.nil?
				print "Last Selected Cell: (" << "x:" << last_cell[:x].to_s << ",y:" << last_cell[:y].to_s << ")"
				print " - adjacents: " << last_cell[:adjacent_mines_count].to_s if last_cell[:mine] == false
				print " EXPLOSIVE!".red if last_cell[:mine] == true
				print "\n"
			end

			print "Discovered Cells: " << valid_discoveries_count.to_s << " - "
			print "Flags: " << flags_count.to_s << " - "
			print "Remaining Cells: " << remaining_cells.to_s

			if arr_mined_cells.count > 1
				print "\n"
				print "The explosive cells coordinates are: |"
				arr_mined_cells.each do |mined_cell|
					print "x:" << mined_cell[:x].to_s << ", y:" << mined_cell[:y].to_s << "|"
				end
			end

			print "\n=========================================================\n"
		end
	end
end