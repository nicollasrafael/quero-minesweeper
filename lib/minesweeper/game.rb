module Minesweeper
	class Game
		def initialize(rows: nil, cols: nil, num_mines: nil)
			# Field properties
			@num_rows = rows
			@num_cols = cols
			@num_cels = rows * cols
			@num_mines = num_mines

			# Game properties
			@valid_game = true
			@valid_discoveries = 0
			@flags = 0
			@last_cell = nil

			# Check the minimum and maximum rows and cols
			raise "rows must be greater than 0 and less than or equal to 100" if @num_rows <= 0 || @num_rows > 100
			raise "cols must be greater than 0 and less than or equal to 100" if @num_cols <= 0 || @num_cols > 100

			@field = construct_field

			# In this version it is necessary to ask for a minimum percentage of mines in the field
			# to prevent SystemStackError ( not 100% )
			min_pct = 0.1 # 10%
			min_mines = (@num_cels.to_f * min_pct).to_i
			raise "num_mines must be greater than or equal to (" << min_mines.to_s << ")" if min_mines > @num_mines

			# Let's also check the maximum ;)
			raise "num_mines must be less than or equal to (" << (@num_cels - 1).to_s << ")" if @num_mines > (@num_cels - 1)
		end

		def play(x:, y:)
			return false if x < 1 || x > @num_cols
			return false if y < 1 || y > @num_rows

			press_cell(x, y)
		end

		def flag(x:, y:)
			return false if x < 1 || x > @num_cols
			return false if y < 1 || y > @num_rows

			toggle_flag(x, y)
		end

		def still_playing?
			@valid_game == true
		end

		def victory?
			@valid_game == false && victory_state?
		end

		def board_state(xray: false)
			field = @field

			if xray == false || @valid_game == true
				field = Marshal.load(Marshal.dump(field))
				field = field.map do |cell|
					cell.delete(:mine) if cell[:discovered] == false
					cell
				end
			end

			{
				field: field,
				num_rows: @num_rows,
				num_cols: @num_cols,
				num_cels: @num_cels,
				num_mines: @num_mines,
				valid_discoveries: @valid_discoveries,
				flags: @flags,
				last_cell: @last_cell
			}
		end

		# Beginning of private methods
		private

		# Press cell
		def press_cell(x, y, expanding: false)
			cell_key = get_cell_key(x, y)

			return false unless @valid_game
			return false if @field[cell_key][:discovered]
			return false if @field[cell_key][:flag]

			# Check the selected cell
			if @field[cell_key][:mine] == true
				@field[cell_key][:discovered] = true
				@valid_game = false
			else
				@field[cell_key][:discovered] = true 
				@valid_discoveries += 1

				# Expand
				if @field[cell_key][:adjacent_mines_count] == 0
					@field[cell_key][:neighbors].each do |nkey|
						cell = @field[nkey]
						next if cell[:discovered] == true || cell[:mine] == true || cell[:flag] == true
						press_cell(cell[:x], cell[:y], expanding: true)
					end
				end

				@valid_game = false if victory_state?
			end

			if expanding == false
				@last_cell = @field[cell_key]
			end

			return true
		end

		def victory_state?
			((@num_cels - @num_mines) == @valid_discoveries) && @valid_discoveries > 0
		end

		# Add or remove the cell flag
		def toggle_flag(x, y)
			cell_key = get_cell_key(x, y)

			return false unless @valid_game
			return false if @field[cell_key][:discovered]
			
			@field[cell_key][:flag] = !@field[cell_key][:flag]
			if @field[cell_key][:flag] == true
				@flags += 1
			else
				@flags -= 1
			end

			return true
		end

		# Build the field with mines
		def construct_field
			# Build the cell information structure one-by-one
			arr_field = Array.new(@num_cels) do |key|
				{
					x: nil,
					y: nil,
					key: key,
					discovered: false,
					flag: false,
					mine: false,
					neighbors: [],
					adjacent_mines_count: 0
				}
			end

			# Shuffle the field and put the mines
			$placed_mines = 0
			arr_field.shuffle!
			arr_field.map! do |cell|
				break if $placed_mines >= @num_mines
				$placed_mines += 1
				cell[:mine] = true
				cell
			end

			# Return field to original position
			arr_field.sort_by! { |val| val[:key] }

			# Register cell coordinates
			(1..@num_rows).each do |y|
				next if y > @num_rows
				(1..@num_cols).each do |x|
					cel_key = get_cell_key(x, y)

					arr_field[cel_key][:x] = x
					arr_field[cel_key][:y] = y

					# Register cell neighbors
					((y - 1)..(y + 1)).each do |iy|
						next if iy <= 0 || iy > @num_rows
						((x - 1)..(x + 1)).each do |ix|
							next if ix == x && iy == y
							next if ix <= 0 || ix > @num_cols
							neighbor_cell_key = get_cell_key(ix, iy)
							arr_field[cel_key][:neighbors] << neighbor_cell_key
						end
					end

					# Register adjacent mines
					adjacent_mines_count = 0
					arr_field[cel_key][:neighbors].each do |neighbor_cell_key|
						adjacent_mines_count += 1 if arr_field[neighbor_cell_key][:mine] == true
					end
					arr_field[cel_key][:adjacent_mines_count] = adjacent_mines_count

					next if x >= @num_cols
				end
			end

			arr_field
		end

		# Calculate the cell key
		def get_cell_key(x, y)
			(((y * @num_cols) - 1) - @num_cols) + x
		end
	end
end