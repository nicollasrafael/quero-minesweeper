require 'colorize'

module Minesweeper
	class PrettyPrinter
		def self.show(board_state, board_format = {})
			# Makes word replacement if informed
			board_format = {
				unknown_cell: board_format[:unknown_cell].nil? ? "." : board_format[:unknown_cell],
				clear_cell: board_format[:clear_cell].nil? ? false : board_format[:clear_cell],
				bomb: board_format[:bomb].nil? ? '#' : board_format[:bomb],
				flag: board_format[:flag].nil? ? 'F' : board_format[:flag]
			}

			valid_discoveries_count = board_state[:valid_discoveries]
			num_cels = board_state[:num_cels]
			num_mines = board_state[:num_mines]
			remaining_cells = (num_cels - num_mines) - valid_discoveries_count

			# Draw the lines
			$line_break = 0
			board_state[:field].each do |field|

				if field[:flag] == true
					if field[:mine] == true
						print board_format[:flag].red
					else
						print board_format[:flag].blue
					end
				elsif field[:mine] == true
					if field[:discovered] == false
						if remaining_cells == 0
							print board_format[:bomb].green
						else
							print board_format[:bomb].red
						end
					else
						print board_format[:bomb].yellow
					end
				elsif field[:discovered] == false
					print board_format[:unknown_cell]
				elsif field[:discovered] == true
					if board_format[:clear_cell] == false
						if not field[:adjacent_mines_count].nil?
							print field[:adjacent_mines_count].to_s
						else
							print '*'
						end
					else
						print board_format[:clear_cell]
					end
				end

				$line_break += 1
				if $line_break >= board_state[:num_cols]
					$line_break = 0
					print "\n"
				end
			end

			print "\n"
		end
	end
end